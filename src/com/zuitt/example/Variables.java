package com.zuitt.example;

public class Variables {

    public static void main(String[] args) {
        //comment
        // Declaration of identifier
        int age;
        char middleName;

        // Variable declaration vs Initialization
        int x;
        int y = 0;

        // Initialization after declaration
        x = 1;

    System.out.println("The value of y is "+ y + "and the value of x is " + x);

    // int - whole number values
    int wholeNumber = 100;
    System.out.println(wholeNumber);

    //long
    //L is added to the end of the Long Number to be recognized

    long worldPop= 78622855154156L;
    System.out.println(worldPop);

    //Floats
    // holds Seven (7) decimal places
    //Float - adding an f at the end of the float to be recognized.
    float piFloat = 3.14159265359f;
    System.out.println(piFloat);

    // holds more than (7) decimal places
    double piDouble =3.14159265359;
    System.out.println(piDouble);

    //char - single character
    //uses single quotes
    char letter ='a';
    System.out.println(letter);

    //boolean - true or false value
    boolean isLove = true;
    boolean isTaken = false;
    System.out.println(isLove);
    System.out.println(isTaken);

    //Constant
    // best practice is to capitalize
    final int PRINCIPAL = 3000;
    System.out.println(PRINCIPAL);

    //Non - primitive Data Type
    // Also known as reference data types referring to instances or objects
    // Do not directly store the value of a variable , but rather remembers the reference of that value.

    //String
    //Stores a sequence or array characters.
    //Strings are actually objects that can use methods.

    String username="JSmith";
    System.out.println(username);

    int stringLength = username.length();
    System.out.println(stringLength);

    }



}
