package com.zuitt.wdc0043;

import java.util.Scanner;

public class User {
    public static void main(String[] args) {
        Scanner myobj = new Scanner(System.in);

        System.out.println("First Name");
        String fnam = myobj.nextLine();

        System.out.println("Last Name");
        String lnam = myobj.nextLine();

        System.out.println("First Grade Subject");
        double num1 = myobj.nextDouble();

        System.out.println("Second Grade Subject");
        double num2 = myobj.nextDouble();

        System.out.println("Third Grade Subject");
        double num3 = myobj.nextDouble();

        System.out.println("Good Day, " + fnam + " " + lnam + ".");
        System.out.println("Your grade average is: " + ((num1+num2+num3)/3));

    }
}
